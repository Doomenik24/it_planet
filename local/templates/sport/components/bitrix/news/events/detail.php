<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?$ElementID = $APPLICATION->IncludeComponent(
	"maximaster:news.detail",
	"events",
	Array(
		"ACTIVE_DATE_FORMAT" => $arParams['DETAIL_ACTIVE_DATE_FORMAT'],
		"AJAX_MODE" => $arParams['AJAX_MODE'],
		"ADD_ELEMENT_CHAIN" => $arParams['ADD_ELEMENT_CHAIN'],
		"ADD_SECTIONS_CHAIN" => $arParams['ADD_SECTIONS_CHAIN'],
		"CACHE_FILTER" => $arParams['CACHE_FILTER'],
		"CACHE_GROUPS" => $arParams['CACHE_GROUPS'],
		"CACHE_TIME" => $arParams['CACHE_TIME'],
		"CACHE_TYPE" => $arParams['CACHE_TYPE'],
		"CHECK_DATES" => $arParams['CHECK_DATES'],
		"COMPONENT_TEMPLATE" => $arParams['COMPONENT_TEMPLATE'],
		"DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["detail"],
		"DISPLAY_BOTTOM_PAGER" => $arParams['DISPLAY_BOTTOM_PAGER'],
		"DISPLAY_DATE" => $arParams['DISPLAY_DATE'],
		"DISPLAY_NAME" => $arParams['DISPLAY_NAME'],
		"DISPLAY_PICTURE" => $arParams['DISPLAY_PICTURE'],
		"DISPLAY_PREVIEW_TEXT" => $arParams['DISPLAY_PREVIEW_TEXT'],
		"DISPLAY_TOP_PAGER" => $arParams['DISPLAY_TOP_PAGER'],
		"FIELD_CODE" => $arParams["DETAIL_FIELD_CODE"],
		"FILTER_NAME" => $arParams['FILTER_NAME'],
		"HIDE_LINK_WHEN_NO_DETAIL" => $arParams['HIDE_LINK_WHEN_NO_DETAIL'],
		"IBLOCK_CODE" => $arParams['IBLOCK_CODE'],
		"IBLOCK_TYPE" => $arParams['IBLOCK_TYPE'],
		"INCLUDE_IBLOCK_INTO_CHAIN" => $arParams['INCLUDE_IBLOCK_INTO_CHAIN'],
		"INCLUDE_SUBSECTIONS" => $arParams['INCLUDE_SUBSECTIONS'],
		"MESSAGE_404" => $arParams['MESSAGE_404'],
		"PAGER_BASE_LINK_ENABLE" => $arParams['PAGER_BASE_LINK_ENABLE'],
		"PAGER_DESC_NUMBERING" => $arParams['PAGER_DESC_NUMBERING'],
		"PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams['PAGER_DESC_NUMBERING_CACHE_TIME'],
		"PAGER_SHOW_ALL" => $arParams['PAGER_SHOW_ALL'],
		"PAGER_SHOW_ALWAYS" => $arParams['PAGER_SHOW_ALWAYS'],
		"PAGER_TEMPLATE" => $arParams['PAGER_TEMPLATE'],
		"PAGER_TITLE" => $arParams['PAGER_TITLE'],
		"PARENT_SECTION" => $arParams['PARENT_SECTION'],
		"PARENT_SECTION_CODE" => $arParams['PARENT_SECTION_CODE'],
		"PREVIEW_TRUNCATE_LEN" => $arParams['PREVIEW_TRUNCATE_LEN'],
		"PROPERTY_CODE" => $arParams["DETAIL_PROPERTY_CODE"],
		"SET_BROWSER_TITLE" => $arParams['SET_BROWSER_TITLE'],
		"SET_LAST_MODIFIED" => $arParams['SET_LAST_MODIFIED'],
		"SET_META_DESCRIPTION" => "Y",
		"SET_META_KEYWORDS" => "Y",
		"SET_STATUS_404" => $arParams['SET_STATUS_404'],
		"SET_TITLE" => $arParams['SET_TITLE'],
		"SHOW_404" => $arParams['SHOW_404'],
		"SORT_BY1" => $arParams['SORT_BY1'],
		"SORT_BY2" => $arParams['SORT_BY2'],
		"SORT_ORDER1" => $arParams['SORT_ORDER1'],
		"SORT_ORDER2" => $arParams['SORT_ORDER2'],
		"ELEMENT_CODE" => $arResult["VARIABLES"]["ELEMENT_CODE"]
	), $component
); ?>
<p><a href="<?=$arResult["FOLDER"]?>"><?=GetMessage("T_NEWS_DETAIL_BACK")?></a></p>
<?if($arParams["USE_RATING"]=="Y" && $ElementID):?>
<?$APPLICATION->IncludeComponent(
	"bitrix:iblock.vote",
	"",
	Array(
		"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
		"ELEMENT_ID" => $ElementID,
		"MAX_VOTE" => $arParams["MAX_VOTE"],
		"VOTE_NAMES" => $arParams["VOTE_NAMES"],
		"CACHE_TYPE" => $arParams["CACHE_TYPE"],
		"CACHE_TIME" => $arParams["CACHE_TIME"],
	),
	$component
);?>
<?endif?>
<?if($arParams["USE_CATEGORIES"]=="Y" && $ElementID):
	global $arCategoryFilter;
	$obCache = new CPHPCache;
	$strCacheID = $componentPath.LANG.$arParams["IBLOCK_ID"].$ElementID.$arParams["CATEGORY_CODE"];
	if(($tzOffset = CTimeZone::GetOffset()) <> 0)
		$strCacheID .= "_".$tzOffset;
	if($arParams["CACHE_TYPE"] == "N" || $arParams["CACHE_TYPE"] == "A" && COption::GetOptionString("main", "component_cache_on", "Y") == "N")
		$CACHE_TIME = 0;
	else
		$CACHE_TIME = $arParams["CACHE_TIME"];
	if($obCache->StartDataCache($CACHE_TIME, $strCacheID, $componentPath))
	{
		$rsProperties = CIBlockElement::GetProperty($arParams["IBLOCK_ID"], $ElementID, "sort", "asc", array("ACTIVE"=>"Y","CODE"=>$arParams["CATEGORY_CODE"]));
		$arCategoryFilter = array();
		while($arProperty = $rsProperties->Fetch())
		{
			if(is_array($arProperty["VALUE"]) && count($arProperty["VALUE"])>0)
			{
				foreach($arProperty["VALUE"] as $value)
					$arCategoryFilter[$value]=true;
			}
			elseif(!is_array($arProperty["VALUE"]) && strlen($arProperty["VALUE"])>0)
				$arCategoryFilter[$arProperty["VALUE"]]=true;
		}
		$obCache->EndDataCache($arCategoryFilter);
	}
	else
	{
		$arCategoryFilter = $obCache->GetVars();
	}
	if(count($arCategoryFilter)>0):
		$arCategoryFilter = array(
			"PROPERTY_".$arParams["CATEGORY_CODE"] => array_keys($arCategoryFilter),
			"!"."ID" => $ElementID,
		);
		?>
		<hr /><h3><?=GetMessage("CATEGORIES")?></h3>
		<?foreach($arParams["CATEGORY_IBLOCK"] as $iblock_id):?>
			<?$APPLICATION->IncludeComponent(
				"bitrix:news.list",
				$arParams["CATEGORY_THEME_".$iblock_id],
				Array(
					"IBLOCK_ID" => $iblock_id,
					"NEWS_COUNT" => $arParams["CATEGORY_ITEMS_COUNT"],
					"SET_TITLE" => "N",
					"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
					"CACHE_TYPE" => $arParams["CACHE_TYPE"],
					"CACHE_TIME" => $arParams["CACHE_TIME"],
					"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
					"FILTER_NAME" => "arCategoryFilter",
					"CACHE_FILTER" => "Y",
					"DISPLAY_TOP_PAGER" => "N",
					"DISPLAY_BOTTOM_PAGER" => "N",
				),
				$component
			);?>
		<?endforeach?>
	<?endif?>
<?endif?>
<?if($arParams["USE_REVIEW"]=="Y" && IsModuleInstalled("forum") && $ElementID):?>
<hr />
<?$APPLICATION->IncludeComponent(
	"bitrix:forum.topic.reviews",
	"",
	Array(
		"CACHE_TYPE" => $arParams["CACHE_TYPE"],
		"CACHE_TIME" => $arParams["CACHE_TIME"],
		"MESSAGES_PER_PAGE" => $arParams["MESSAGES_PER_PAGE"],
		"USE_CAPTCHA" => $arParams["USE_CAPTCHA"],
		"PATH_TO_SMILE" => $arParams["PATH_TO_SMILE"],
		"FORUM_ID" => $arParams["FORUM_ID"],
		"URL_TEMPLATES_READ" => $arParams["URL_TEMPLATES_READ"],
		"SHOW_LINK_TO_FORUM" => $arParams["SHOW_LINK_TO_FORUM"],
		"DATE_TIME_FORMAT" => $arParams["DETAIL_ACTIVE_DATE_FORMAT"],
		"ELEMENT_ID" => $ElementID,
		"AJAX_POST" => $arParams["REVIEW_AJAX_POST"],
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
		"URL_TEMPLATES_DETAIL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["detail"],
	),
	$component
);?>
<?endif?>
