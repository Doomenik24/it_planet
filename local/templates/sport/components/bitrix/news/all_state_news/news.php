<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<? $APPLICATION->IncludeComponent(
    "maximaster:news.list",
    "all_state_news",
    Array(
        "ACTIVE_DATE_FORMAT" => $arParams['LIST_ACTIVE_DATE_FORMAT'],
        "ADD_ELEMENT_CHAIN" => $arParams['ADD_ELEMENT_CHAIN'],
        "ADD_SECTIONS_CHAIN" => $arParams['ADD_SECTIONS_CHAIN'],
        "CACHE_FILTER" => $arParams['CACHE_FILTER'],
        "CACHE_GROUPS" => $arParams['CACHE_GROUPS'],
        "CACHE_TIME" => $arParams['CACHE_TIME'],
        "CACHE_TYPE" => $arParams['CACHE_TYPE'],
        "CHECK_DATES" => $arParams['CHECK_DATES'],
        "COMPONENT_TEMPLATE" => $arParams['COMPONENT_TEMPLATE'],
        "DETAIL_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["detail"],
        "DISPLAY_BOTTOM_PAGER" => $arParams['DISPLAY_BOTTOM_PAGER'],
        "DISPLAY_DATE" => $arParams['DISPLAY_DATE'],
        "DISPLAY_NAME" => $arParams['DISPLAY_NAME'],
        "DISPLAY_PICTURE" => $arParams['DISPLAY_PICTURE'],
        "DISPLAY_PREVIEW_TEXT" => $arParams['DISPLAY_PREVIEW_TEXT'],
        "DISPLAY_TOP_PAGER" => $arParams['DISPLAY_TOP_PAGER'],
        "FIELD_CODE" => $arParams['LIST_FIELD_CODE'],
        "FILTER_NAME" => $arParams['FILTER_NAME'],
        "HIDE_LINK_WHEN_NO_DETAIL" => $arParams['HIDE_LINK_WHEN_NO_DETAIL'],
        "IBLOCK_CODE" => $arParams['IBLOCK_CODE'],
        "IBLOCK_TYPE" => $arParams['IBLOCK_TYPE'],
        "INCLUDE_IBLOCK_INTO_CHAIN" => $arParams['INCLUDE_IBLOCK_INTO_CHAIN'],
        "INCLUDE_SUBSECTIONS" => $arParams['INCLUDE_SUBSECTIONS'],
        "MESSAGE_404" => $arParams['MESSAGE_404'],
        "NEWS_COUNT" => $arParams['NEWS_COUNT'],
        "PAGER_BASE_LINK_ENABLE" => $arParams['PAGER_BASE_LINK_ENABLE'],
        "PAGER_DESC_NUMBERING" => $arParams['PAGER_DESC_NUMBERING'],
        "PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams['PAGER_DESC_NUMBERING_CACHE_TIME'],
        "PAGER_SHOW_ALL" => $arParams['PAGER_SHOW_ALL'],
        "PAGER_SHOW_ALWAYS" => $arParams['PAGER_SHOW_ALWAYS'],
        "PAGER_TEMPLATE" => $arParams['PAGER_TEMPLATE'],
        "PAGER_TITLE" => $arParams['PAGER_TITLE'],
        "PARENT_SECTION" => $arParams['PARENT_SECTION'],
        "PARENT_SECTION_CODE" => $arParams['PARENT_SECTION_CODE'],
        "PREVIEW_TRUNCATE_LEN" => $arParams['PREVIEW_TRUNCATE_LEN'],
        "PROPERTY_CODE" => $arParams['LIST_PROPERTY_CODE'],
        "SET_BROWSER_TITLE" => $arParams['SET_BROWSER_TITLE'],
        "SET_LAST_MODIFIED" => $arParams['SET_LAST_MODIFIED'],
        "SET_META_DESCRIPTION" => "Y",
        "SET_META_KEYWORDS" => "Y",
        "SET_STATUS_404" => $arParams['SET_STATUS_404'],
        "SET_TITLE" => $arParams['SET_TITLE'],
        "SHOW_404" => $arParams['SHOW_404'],
        "SORT_BY1" => $arParams['SORT_BY1'],
        "SORT_BY2" => $arParams['SORT_BY2'],
        "SORT_ORDER1" => $arParams['SORT_ORDER1'],
        "SORT_ORDER2" => $arParams['SORT_ORDER2'],
    )
); ?>