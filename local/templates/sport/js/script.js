$(document).ready(function () {

    var arFilter = {};

    function getNews(arFilter) {
        $.ajax({
            type: "POST",
            url: arFilter.url,
            data: ({
                'ID': arFilter.id
            }),
            success: function (msg) {

                $('.news-list').replaceWith(msg);

                if ($('.news-list .news_item').length == 0) {
                    $('.news-list').html(
                        '<p>Новости для данного региона отсутствуют</p>'
                    );
                }
            }
        });
    }


    if ('.content .news-list'.length > 0) {
        arFilter.url = '/index.php';
        arFilter.id = $('.regions').val();
    } else if ('.all_news'.length > 0) {
        arFilter.url = '/news/index.php';
        arFilter.id = $('.regions').val();
    }

    getNews(arFilter);

    $('.regions').on('change', function () {
        if ('.content .news-list'.length > 0) {
            arFilter.url = '/index.php';
            arFilter.id = $('.regions').val();
        } else if ('.all_news'.length > 0) {
            arFilter.url = '/news/index.php';
            arFilter.id = $('.regions').val();
        }
        getNews(arFilter);
    });

    $('.slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000
    });

    /* карта */

    if ($('.js-yandex-filtrable-map').length > 0) {
        initYandexMap();
    }

    function initYandexMap() {
        var myMap,
            arContentMap = {},
            iconColor = '';

        function setContentMap() {
            var
                pointID;
            /* ID точки */

            arContentMap = {};

            /* Массив данных для балуна */
            $.get('/ajax/getContentMap.php', {action: 'getContentMap'}, function (data) {
                $json = $.parseJSON(data);
                for (pointID in $json) {

                    if ($json[pointID]['ADDRESS']) {
                        $json[pointID]['ADDRESS'] = $json[pointID]['ADDRESS'].split(',');
                    }

                    arContentMap[pointID] = $json[pointID];
                }

                if (myMap) {
                    myMap.destroy();
                }
                generateYandexMaps(arContentMap);
            });

        }

        setContentMap();
        ymaps.ready(generateYandexMaps);

        function generateYandexMaps() {
            var myGeoObjects = new ymaps.GeoObjectCollection({}, {
                preset: "islands#icon",
                strokeWidth: 4,
                geodesic: true
            });

            myMap = new ymaps.Map("map",
                {
                    center: [54.193397196679165, 37.61645257934563],
                    controls: ['zoomControl', 'fullscreenControl'],
                    zoom: 14
                },
                {
                    maxZoom: 18,
                    minZoom: 1
                });
            for (idPoint in arContentMap) {
                var contentHeader = '',
                    contentBody = '', contentFooter = '';

                if (arContentMap[idPoint]['NAME']) {
                    contentHeader = arContentMap[idPoint]['NAME'];
                }

                if (arContentMap[idPoint]['PREVIEW_TEXT']) {
                    contentBody += 'Краткое описание: ' + arContentMap[idPoint]['PREVIEW_TEXT'] + '<br>';
                }

                if (arContentMap[idPoint]['TYPE']) {
                    contentBody += 'Тип площадки: ' + arContentMap[idPoint]['TYPE'] + '<br>';
                } else {
                    contentFooter = 'Чтобы записаться в секцию перейдите по ссылке:';
                }

                if (arContentMap[idPoint]['WORK_TIME']) {
                    contentBody += 'Время работы: ' + arContentMap[idPoint]['WORK_TIME'] + '<br>';
                }

                if (arContentMap[idPoint]['EMAIL']) {
                    contentBody += 'Электронная почта: ' + arContentMap[idPoint]['EMAIL'] + '<br>';
                }

                if (arContentMap[idPoint]['TYPE']) {
                    iconColor = '#a5260a';
                } else {
                    iconColor = '#0095b6';
                }

                if (arContentMap[idPoint]['ADDRESS']) {
                    myGeoObjects.add(new ymaps.Placemark([
                            arContentMap[idPoint]['ADDRESS']['0'],
                            arContentMap[idPoint]['ADDRESS']['1']
                        ],
                        {
                            balloonContentHeader: contentHeader,
                            balloonContentBody: contentBody,
                            balloonContentFooter: contentFooter
                        },
                        {
                            iconColor: iconColor
                        }
                    ));

                    myMap.geoObjects.add(myGeoObjects);
                    myMap.setBounds(myGeoObjects.getBounds());
                }
            }
        }
    }
});
