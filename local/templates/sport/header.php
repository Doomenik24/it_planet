<?
use \Bitrix\Main\Page\Asset;
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
    <head>
        <?$APPLICATION->ShowHead();?>

        <title><?$APPLICATION->ShowTitle();?></title>

        <link rel="shortcut icon" type="image/x-icon" href="<?=SITE_TEMPLATE_PATH?>/images/favicon.ico" />
        <?
             Asset::getInstance()->addJs("https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js");
             Asset::getInstance()->addJs("https://api-maps.yandex.ru/2.1/?lang=ru_RU");
             Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/slick/slick.js");
             Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/script.js");


             Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/bootstrap.css");
             Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/components.css");
             Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/style.css");
             Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/select_region.css");
             Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/news_list.css");
             Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/js/slick/slick.css");
             Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/js/slick/slick-theme.css");
        ?>

        <script>
            $(function() {
                var pull = $('#pull');
                menu = $('nav ul');
                menuHeight = menu.height();
                $(pull).on('click', function(e) {
                    e.preventDefault();
                    menu.slideToggle();
                });
                $(window).resize(function(){
                    var w = $(window).width();
                    if(w > 320 && menu.is(':hidden')) {
                        menu.removeAttr('style');
                    }
                });
            });
        </script>
    </head>
    <body>
        <?$APPLICATION->ShowPanel();?>

        <div id="home" class="header">
            <div class="container">
                <div class="top_header">
                    <div class="select_region">
                        <?$APPLICATION->IncludeComponent(
                            "maximaster:select_region",
                            "",
                            Array(
                                "COMPONENT_TEMPLATE" => ".default"
                            )
                        );?>
                    </div>

                    <div class="profile">
                        <?
                        $APPLICATION->IncludeComponent(
                            "bitrix:system.auth.form",
                            "header",
                            Array(
                                "COMPONENT_TEMPLATE" => "header",
                                "FORGOT_PASSWORD_URL" => "/forgot_password/",
                                "PROFILE_URL" => "/profile/",
                                "REGISTER_URL" => "/register/",
                                "SHOW_ERRORS" => "N"
                            )
                        );?>
                    </div>
                </div>

                <div class="clearfix"> </div>

                <?$APPLICATION->IncludeComponent(
                    "bitrix:menu",
                    "top_menu",
                    Array(
                        "ALLOW_MULTI_SELECT" => "N",
                        "CHILD_MENU_TYPE" => "left",
                        "COMPONENT_TEMPLATE" => "vertical_multilevel",
                        "DELAY" => "N",
                        "MAX_LEVEL" => "1",
                        "MENU_CACHE_GET_VARS" => array(""),
                        "MENU_CACHE_TIME" => "3600",
                        "MENU_CACHE_TYPE" => "N",
                        "MENU_CACHE_USE_GROUPS" => "Y",
                        "ROOT_MENU_TYPE" => "top",
                        "USE_EXT" => "N"
                    )
                );?>
            </div>
        </div>
