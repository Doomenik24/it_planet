<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<? foreach ($arResult['ITEM_LIST']['ITEMS'] as $sCodeSection => $arItems): ?>
    <table>
        <tr style="text-align: center">
            <td colspan="13"><?= $sCodeSection ?></td>
        </tr>
        <tr>
            <td>Дистанция</td>
            <? foreach ($arItems['0']['PROPERTIES'] as $arItem): ?>
                <? if ($arItem['NAME']): ?>
                    <td><?= $arItem['NAME'] ?></td>
                <? endif ?>
            <? endforeach ?>
        </tr>
        <? foreach ($arItems as $arItem): ?>
            <?
            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
            ?>
            <tr>
                <? if ($arItem['NAME']): ?>
                    <td><?= $arItem['NAME'] ?></td>
                <? endif; ?>
                <? foreach ($arItem['PROPERTIES'] as $sCodeProperties => $arProps): ?>
                    <td><?= $arProps['VALUE'] ?></td>
                <? endforeach ?>
            </tr>
        <? endforeach; ?>
    </table>
<? endforeach ?>