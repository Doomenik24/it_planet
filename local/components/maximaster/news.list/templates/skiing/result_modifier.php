<?
/* соберем массив таким образом, чтобы элементы принадлежали конкретному разделу */
foreach ($arResult['ITEMS'] as $arItem) {
    $arSectionID['SECTION_ID'][$arItem['IBLOCK_SECTION_ID']] = $arItem['IBLOCK_SECTION_ID'];
}
$arFilter = Array('ID' => $arSectionID['SECTION_ID']);
$db_list = CIBlockSection::GetList(Array("SORT" => "ASC"), $arFilter, true);
while ($ar_result = $db_list->Fetch()) {
    $arSection[$ar_result['ID']] = $ar_result['NAME'];
}
foreach ($arResult['ITEMS'] as $arItem) {
    if ($arSection[$arItem['IBLOCK_SECTION_ID']]) {
        $arResult['ITEM_LIST']['ITEMS'][$arSection[$arItem['IBLOCK_SECTION_ID']]][] = $arItem;
    }
}