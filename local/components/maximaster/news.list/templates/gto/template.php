<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<? foreach ($arResult['ITEM_LIST']['ITEMS'] as $sNameParentSection => $arChildrenSection): ?>
    <table style="min-width: 400px; min-height: 100px; margin: 0 auto; max-width: 600px;">
        <tr>
            <td style="font-size: 20px; font-weight: bold; padding: 5px;" colspan="5">
                <?= $sNameParentSection ?>
            </td>
        </tr>
        <? foreach ($arChildrenSection as $sCodeSection => $arItems): ?>
            <tr style="text-align: center">
                <td colspan="5" style="padding: 5px; color: red;">Возраст: <?= $sCodeSection ?></td>
            </tr>
            <tr>
                <td style="padding: 5px;">№ п/п</td>
                <? foreach ($arItems['0']['PROPERTIES'] as $arItem): ?>
                    <? if ($arItem['NAME']): ?>
                        <td style="padding: 5px;"><?= $arItem['NAME'] ?></td>
                    <? endif ?>
                <? endforeach ?>
            </tr>
            <? foreach ($arItems as $arItem): ?>
                <?
                $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                ?>
                <tr>
                    <? if ($arItem['NAME']): ?>
                        <td style="padding: 5px;"><?= $arItem['NAME'] ?></td>
                    <? endif; ?>
                    <? foreach ($arItem['PROPERTIES'] as $sCodeProperties => $arProps): ?>
                        <? if ($arProps['CODE'] == 'TYPE_TEST'): ?>
                            <td style="padding: 5px;">
                                <a style="text-decoration: underline" href="/gto/type_test/<?= $arProps['CODE_TYPE_TEST'] ?>/">
                                    <?= $arProps['VALUE'] ?>
                                </a>
                            </td>
                        <? else: ?>
                            <td style="padding: 5px;"><?= $arProps['VALUE'] ?></td>
                        <? endif ?>
                    <? endforeach ?>
                </tr>
            <? endforeach; ?>
        <? endforeach ?>
    </table>
<? endforeach ?>