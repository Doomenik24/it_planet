<?
/* соберем массив таким образом, чтобы элементы принадлежали конкретному разделу */
foreach ($arResult['ITEMS'] as $arItem) {
    $arSectionID['SECTION_ID'][$arItem['IBLOCK_SECTION_ID']] = $arItem['IBLOCK_SECTION_ID'];
    if ($arItem['PROPERTIES']['TYPE_TEST']) {
        $arPropertyTypeTest['ELEMENT_ID'][$arItem['ID']] = $arItem['ID'];
        $arPropertyTypeTest['PROPERTY_ID'][$arItem['PROPERTIES']['TYPE_TEST']['VALUE']] = $arItem['PROPERTIES']['TYPE_TEST']['VALUE'];
    }
}
/* получим данные о привязанном свойстве */
$dbRes = CIBlockElement::GetList(Array(), array('ID' => $arPropertyTypeTest['PROPERTY_ID']), false, false, array('ID', 'NAME', 'CODE'));
while ($arRes = $dbRes->Fetch()) {
    $arPropertyTypeTest['PROPERTY_TYPE_TEST'][$arRes['ID']]['NAME'] = $arRes['NAME'];
    $arPropertyTypeTest['PROPERTY_TYPE_TEST'][$arRes['ID']]['CODE'] = $arRes['CODE'];
}

$arFilter = Array('ID' => $arSectionID['SECTION_ID']);
$db_list = CIBlockSection::GetList(Array("SORT" => "ASC"), $arFilter, true);
while ($ar_result = $db_list->Fetch()) {
    $arSection['ID_PARENT_SECTION'][$ar_result['IBLOCK_SECTION_ID']][$ar_result['ID']] = $ar_result['NAME'];
    $arIdParentSection[$ar_result['IBLOCK_SECTION_ID']] = $ar_result['IBLOCK_SECTION_ID'];
}
/* получаем родительский раздел */
$db_list = CIBlockSection::GetList(Array("SORT" => "ASC"), array('ID' => $arIdParentSection), true);
while ($ar_result = $db_list->Fetch()) {
    $arParentSection[$ar_result['ID']] = $ar_result['NAME'];
}
foreach ($arParentSection as $nIdParentSection => $sNameParentSection) {
    if ($arSection['ID_PARENT_SECTION'][$nIdParentSection]) {
        foreach ($arResult['ITEMS'] as $arItem) {
            if ($arPropertyTypeTest['PROPERTY_TYPE_TEST'][$arItem['PROPERTIES']['TYPE_TEST']['VALUE']]) {
                $arItem['PROPERTIES']['TYPE_TEST']['CODE_TYPE_TEST'] = $arPropertyTypeTest['PROPERTY_TYPE_TEST'][$arItem['PROPERTIES']['TYPE_TEST']['VALUE']]['CODE'];
                $arItem['PROPERTIES']['TYPE_TEST']['VALUE'] = $arPropertyTypeTest['PROPERTY_TYPE_TEST'][$arItem['PROPERTIES']['TYPE_TEST']['VALUE']]['NAME'];
                $arItem['PROPERTIES']['TYPE_TEST']['~VALUE'] = $arPropertyTypeTest['PROPERTY_TYPE_TEST'][$arItem['PROPERTIES']['TYPE_TEST']['VALUE']]['NAME'];

            }
            if ($arSection['ID_PARENT_SECTION'][$nIdParentSection][$arItem['IBLOCK_SECTION_ID']]) {
                $arResult['ITEM_LIST']['ITEMS'][$sNameParentSection][$arSection['ID_PARENT_SECTION'][$nIdParentSection][$arItem['IBLOCK_SECTION_ID']]][] = $arItem;
            }
        }
    }
}