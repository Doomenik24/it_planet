<?php
foreach ($arResult['ITEMS'] as $key => $arItem) {
    if ($arItem['PREVIEW_PICTURE']) {
        $arFileTmp = CFile::ResizeImageGet(
            $arItem["PREVIEW_PICTURE"],
            array(
                "width" => 375,
                "height" => 200),
            BX_RESIZE_IMAGE_EXACT,
            false,
            array() //убираем черный фон у прозрачных изображений
        );
       $arResult['ITEMS'][$key]['PREVIEW_PICTURE']['SRC'] = $arFileTmp['src'];
    }
}