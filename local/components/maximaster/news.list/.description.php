<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => GetMessage('COMPONENT_NAME'),
	"DESCRIPTION" => GetMessage('COMPONENT_DESCRIPTION'),
	"SORT" => 990,
	"CACHE_PATH" => "Y",
	"ICON" => "/images/news.list.gif",
	"PATH" => array(
		"ID" => "Максимастер",
		"NAME" => GetMessage('COMPANY_NAME'),
		"CHILD" => array(
			"ID" => "sport",
			"NAME" => GetMessage('COMPONENT_FOLDER'),
			"SORT" => 10,
		)
	),
);