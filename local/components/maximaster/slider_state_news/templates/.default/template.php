<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<div class="slider">
    <? foreach ($arResult as $key => $arItems): ?>
        <div class="slide">
            <a href="/news/all_state_news/<?=$arItems['CODE']?>/">
                <img src="<?=$arItems['DETAIL_PICTURE']['src']?>">
                <div class="description">
                    <?=$arItems['PREVIEW_TEXT']?>
                    <br>
                    <a style="text-decoration: underline;" href="/news/all_state_news/">Узнать все новости страны</a>
                </div>
            </a>
        </div>
    <? endforeach; ?>
</div>
