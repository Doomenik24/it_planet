<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => GetMessage("SELECT_REGION_NAME"),
	"DESCRIPTION" => GetMessage("SELECT_REGION_DESC"),
	"ICON" => "",
	"CACHE_PATH" => "Y",
	"PATH" => array(
		"ID" => "Максимастер",
		"CHILD" => array(
			"ID" => "sport",
			"NAME" => GetMessage("COMPONENTS_MAXIMASTER")
		)
	),
);
?>