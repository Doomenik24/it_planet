<?php

use Bitrix\Main;
use Bitrix\Main\Localization\Loc;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

Loc::loadMessages(__FILE__);
CModule::IncludeModule('iblock');

class StateNews extends \CBitrixComponent
{
    /**
     * Получим столько новостей, сколько указали в параметрах компонента
     * @return array
     * @throws Main\ArgumentException
     */
    public function getStateNews()
    {
        $arParams = $this->arParams;

        $res = \Bitrix\Iblock\ElementTable::getList(
            array(
                'filter' => array(
                    'IBLOCK_ID' => '7',
                ),
                'select' => array(
                    'ID',
                    'NAME',
                    'PREVIEW_TEXT',
                    'PREVIEW_PICTURE',
                    'DETAIL_TEXT',
                    'DETAIL_PICTURE',
                    'CODE'
                )
            )
        );


        while ($arItems = $res->Fetch()) {

            if ($arItems['DETAIL_PICTURE']) {
                $arItems['DETAIL_PICTURE'] = CFile::ResizeImageGet($arItems["DETAIL_PICTURE"], Array("width" => 800, "height" => 400));
            } elseif ($arItems['PREVIEW_PICTURE']) {
                $arItems['PREVIEW_PICTURE'] = CFile::ResizeImageGet($arItems["PREVIEW_PICTURE"], Array("width" => 800, "height" => 400));
            }

            $arResult[] = $arItems;

        }

        return $arResult;
    }

    public function executeComponent()
    {
        $this->arResult = $this->getStateNews();
        $this->includeComponentTemplate();
    }
}