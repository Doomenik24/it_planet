<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
use Bitrix\Main\Context;
use Bitrix\Main\Type\DateTime;

CBitrixComponent::includeComponentClass("maximaster:news.list");

class NewsDetail extends NewsList
{
	function SetParams($arParams, $DB, $USER)
	{
		if(!isset($arParams["CACHE_TIME"]))
			$arParams["CACHE_TIME"] = 36000000;
		$arParams["PARENT_SECTION"] = intval($arParams["PARENT_SECTION"]);
		$arParams["INCLUDE_SUBSECTIONS"] = $arParams["INCLUDE_SUBSECTIONS"]!="N";
		$arParams["SET_LAST_MODIFIED"] = $arParams["SET_LAST_MODIFIED"]==="Y";

		$arParams["SORT_BY1"] = trim($arParams["SORT_BY1"]);
		if(strlen($arParams["SORT_BY1"])<=0)
			$arParams["SORT_BY1"] = "ACTIVE_FROM";
		if(!preg_match('/^(asc|desc|nulls)(,asc|,desc|,nulls){0,1}$/i', $arParams["SORT_ORDER1"]))
			$arParams["SORT_ORDER1"]="DESC";

		if(strlen($arParams["SORT_BY2"])<=0)
			$arParams["SORT_BY2"] = "SORT";
		if(!preg_match('/^(asc|desc|nulls)(,asc|,desc|,nulls){0,1}$/i', $arParams["SORT_ORDER2"]))
			$arParams["SORT_ORDER2"]="ASC";

		if(strlen($arParams["FILTER_NAME"])<=0 || !preg_match("/^[A-Za-z_][A-Za-z01-9_]*$/", $arParams["FILTER_NAME"]))
		{
			$arrFilter = array();
		}
		else
		{
			$arrFilter = $GLOBALS[$arParams["FILTER_NAME"]];

			if(!is_array($arrFilter))
				$arrFilter = array();
		}
		$arParams["CHECK_DATES"] = $arParams["CHECK_DATES"]!="N";

		if(!is_array($arParams["FIELD_CODE"]))
			$arParams["FIELD_CODE"] = array();
		foreach($arParams["FIELD_CODE"] as $key=>$val)
			if(!$val)
				unset($arParams["FIELD_CODE"][$key]);

		if(!is_array($arParams["PROPERTY_CODE"]))
			$arParams["PROPERTY_CODE"] = array();
		foreach($arParams["PROPERTY_CODE"] as $key=>$val)
			if($val==="")
				unset($arParams["PROPERTY_CODE"][$key]);

		$arParams["DETAIL_URL"]=trim($arParams["DETAIL_URL"]);

		$arParams["CACHE_FILTER"] = $arParams["CACHE_FILTER"]=="Y";
		if(!$arParams["CACHE_FILTER"] && count($arrFilter)>0)
			$arParams["CACHE_TIME"] = 0;

		$arParams["SET_TITLE"] = $arParams["SET_TITLE"]!="N";
		$arParams["SET_BROWSER_TITLE"] = (isset($arParams["SET_BROWSER_TITLE"]) && $arParams["SET_BROWSER_TITLE"] === 'N' ? 'N' : 'Y');
		$arParams["SET_META_KEYWORDS"] = (isset($arParams["SET_META_KEYWORDS"]) && $arParams["SET_META_KEYWORDS"] === 'N' ? 'N' : 'Y');
		$arParams["SET_META_DESCRIPTION"] = (isset($arParams["SET_META_DESCRIPTION"]) && $arParams["SET_META_DESCRIPTION"] === 'N' ? 'N' : 'Y');
		$arParams["ADD_SECTIONS_CHAIN"] = $arParams["ADD_SECTIONS_CHAIN"]!="N"; //Turn on by default
		$arParams["INCLUDE_IBLOCK_INTO_CHAIN"] = $arParams["INCLUDE_IBLOCK_INTO_CHAIN"]!="N";
		$arParams["ACTIVE_DATE_FORMAT"] = trim($arParams["ACTIVE_DATE_FORMAT"]);
		if(strlen($arParams["ACTIVE_DATE_FORMAT"])<=0)
			$arParams["ACTIVE_DATE_FORMAT"] = $DB->DateFormatToPHP(CSite::GetDateFormat("SHORT"));
		$arParams["PREVIEW_TRUNCATE_LEN"] = intval($arParams["PREVIEW_TRUNCATE_LEN"]);
		$arParams["HIDE_LINK_WHEN_NO_DETAIL"] = $arParams["HIDE_LINK_WHEN_NO_DETAIL"]=="Y";

		$arParams["PAGER_TITLE"] = trim($arParams["PAGER_TITLE"]);
		$arParams["PAGER_SHOW_ALWAYS"] = $arParams["PAGER_SHOW_ALWAYS"]=="Y";
		$arParams["PAGER_TEMPLATE"] = trim($arParams["PAGER_TEMPLATE"]);
		$arParams["CHECK_PERMISSIONS"] = $arParams["CHECK_PERMISSIONS"]!="N";


		if (empty($arParams["PAGER_PARAMS_NAME"]) || !preg_match("/^[A-Za-z_][A-Za-z01-9_]*$/", $arParams["PAGER_PARAMS_NAME"]))
		{
			$pagerParameters = array();
		}
		else
		{
			$pagerParameters = $GLOBALS[$arParams["PAGER_PARAMS_NAME"]];
			if (!is_array($pagerParameters))
				$pagerParameters = array();
		}

		$arParams["USE_PERMISSIONS"] = $arParams["USE_PERMISSIONS"]=="Y";
		if(!is_array($arParams["GROUP_PERMISSIONS"]))
			$arParams["GROUP_PERMISSIONS"] = array(1);

		$bUSER_HAVE_ACCESS = !$arParams["USE_PERMISSIONS"];
		if($arParams["USE_PERMISSIONS"] && isset($GLOBALS["USER"]) && is_object($GLOBALS["USER"]))
		{
			$arUserGroupArray = $USER->GetUserGroupArray();
			foreach($arParams["GROUP_PERMISSIONS"] as $PERM)
			{
				if(in_array($PERM, $arUserGroupArray))
				{
					$bUSER_HAVE_ACCESS = true;
					break;
				}
			}
		}
		return array($arParams, $arrFilter, $pagerParameters, $bUSER_HAVE_ACCESS);

	}

	function SelectList($arField, $arProperty)
	{
		$arSelect = array_merge($arField, array(
			"ID",
			"IBLOCK_ID",
			"IBLOCK_SECTION_ID",
			"NAME",
			"ACTIVE_FROM",
			"TIMESTAMP_X",
			"DETAIL_PAGE_URL",
			"LIST_PAGE_URL",
			"DETAIL_TEXT",
			"DETAIL_TEXT_TYPE",
			"PREVIEW_TEXT",
			"PREVIEW_TEXT_TYPE",
			"PREVIEW_PICTURE",
			'CODE'
		));
		if(count($arProperty)>0)
			$arSelect[]="PROPERTY_*";
		return $arSelect;
	}

	function FilterList($arParams, $arrFilter, $arSearchFilter)
	{
		$arFilter = array(
			'ACTIVE' => 'Y',
			"CHECK_PERMISSIONS" => $arParams['CHECK_PERMISSIONS'] ? "Y" : "N",
			'IBLOCK_CODE' => $arParams['IBLOCK_CODE'],
			'SITE_ID' => SITE_ID,
			'CODE' => $arParams['ELEMENT_CODE']
		);
		if($arParams["CHECK_DATES"])
			$arFilter["ACTIVE_DATE"] = "Y";

		$arFilter = array_merge($arFilter, $arrFilter);
		if ($arSearchFilter)
		{
			$arFilter = array_merge($arFilter, $arSearchFilter);
		}
		return array($arParams, $arFilter);
	}

	function ExecuteElements($arParams, $arSort, $arFilter, $arGroup, $arNavParams, $arSelect, $pagerParameters, $bUSER_HAVE_ACCESS)
	{
		$arResult["USER_HAVE_ACCESS"] = $bUSER_HAVE_ACCESS;
		$obParser = new CTextParser;
		$arResult["ITEMS"] = array();
		$arResult["ELEMENTS"] = array();
		$rsElement = CIBlockElement::GetList($arSort, $arFilter, $arGroup, $arNavParams, $arSelect);
		while($obElement = $rsElement->GetNextElement())
		{
			$arItem = $obElement->GetFields();

			$arButtons = CIBlock::GetPanelButtons($arItem["IBLOCK_ID"], $arItem["ID"], 0, array(
					"SECTION_BUTTONS" => false,
					"SESSID" => false
				));
			$arItem["EDIT_LINK"] = $arButtons["edit"]["edit_element"]["ACTION_URL"];
			$arItem["DELETE_LINK"] = $arButtons["edit"]["delete_element"]["ACTION_URL"];

			if ($arParams["PREVIEW_TRUNCATE_LEN"] > 0)
			{
				$arItem["PREVIEW_TEXT"] = $obParser->html_cut($arItem["PREVIEW_TEXT"], $arParams["PREVIEW_TRUNCATE_LEN"]);
			}

			if (strlen($arItem["ACTIVE_FROM"]) > 0)
			{
				$arItem["DISPLAY_ACTIVE_FROM"] = CIBlockFormatProperties::DateFormat($arParams["ACTIVE_DATE_FORMAT"], MakeTimeStamp($arItem["ACTIVE_FROM"], CSite::GetDateFormat()));
			}
			else
			{
				$arItem["DISPLAY_ACTIVE_FROM"] = "";
			}

			$ipropValues = new \Bitrix\Iblock\InheritedProperty\ElementValues($arItem["IBLOCK_ID"], $arItem["ID"]);
			$arItem["IPROPERTY_VALUES"] = $ipropValues->getValues();

			if (isset($arItem["PREVIEW_PICTURE"]))
			{
				$arItem["PREVIEW_PICTURE"] = (0 < $arItem["PREVIEW_PICTURE"] ? CFile::GetFileArray($arItem["PREVIEW_PICTURE"]) : false);
				if ($arItem["PREVIEW_PICTURE"])
				{
					$arItem["PREVIEW_PICTURE"]["ALT"] = $arItem["IPROPERTY_VALUES"]["ELEMENT_PREVIEW_PICTURE_FILE_ALT"];
					if ($arItem["PREVIEW_PICTURE"]["ALT"] == "")
					{
						$arItem["PREVIEW_PICTURE"]["ALT"] = $arItem["NAME"];
					}
					$arItem["PREVIEW_PICTURE"]["TITLE"] = $arItem["IPROPERTY_VALUES"]["ELEMENT_PREVIEW_PICTURE_FILE_TITLE"];
					if ($arItem["PREVIEW_PICTURE"]["TITLE"] == "")
					{
						$arItem["PREVIEW_PICTURE"]["TITLE"] = $arItem["NAME"];
					}
				}
			}

			if (isset($arItem["DETAIL_PICTURE"]))
			{
				$arItem["DETAIL_PICTURE"] = (0 < $arItem["DETAIL_PICTURE"] ? CFile::GetFileArray($arItem["DETAIL_PICTURE"]) : false);
				if ($arItem["DETAIL_PICTURE"])
				{
					$arItem["DETAIL_PICTURE"]["ALT"] = $arItem["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_ALT"];
					if ($arItem["DETAIL_PICTURE"]["ALT"] == "")
					{
						$arItem["DETAIL_PICTURE"]["ALT"] = $arItem["NAME"];
					}
					$arItem["DETAIL_PICTURE"]["TITLE"] = $arItem["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_TITLE"];
					if ($arItem["DETAIL_PICTURE"]["TITLE"] == "")
					{
						$arItem["DETAIL_PICTURE"]["TITLE"] = $arItem["NAME"];
					}
				}
			}

			$arItem["FIELDS"] = array();
			foreach ($arParams["FIELD_CODE"] as $code)
			{
				if (array_key_exists($code, $arItem))
				{
					$arItem["FIELDS"][$code] = $arItem[$code];
				}
			}


			if (count($arParams['PROPERTY_CODE'])>0)
			{
				$arItem["PROPERTIES"] = $obElement->GetProperties();
			}
			$arItem["DISPLAY_PROPERTIES"] = array();
			foreach ($arParams["PROPERTY_CODE"] as $pid)
			{
				$prop = &$arItem["PROPERTIES"][$pid];
				if ((is_array($prop["VALUE"]) && count($prop["VALUE"]) > 0) || (!is_array($prop["VALUE"]) && strlen($prop["VALUE"]) > 0))
				{
					$arItem["DISPLAY_PROPERTIES"][$pid] = CIBlockFormatProperties::GetDisplayValue($arItem, $prop, "news_out");
				}
			}

			if ($arParams["SET_LAST_MODIFIED"])
			{
				$time = DateTime::createFromUserTime($arItem["TIMESTAMP_X"]);
				if (!isset($arResult["ITEMS_TIMESTAMP_X"]) || $time->getTimestamp() > $arResult["ITEMS_TIMESTAMP_X"]->getTimestamp())
				{
					$arResult["ITEMS_TIMESTAMP_X"] = $time;
				}
			}

			$arResult = $arItem;
			$arResult["ELEMENTS"] = $arItem["ID"];
		}
		$navComponentParameters = array();
		if ($arParams["PAGER_BASE_LINK_ENABLE"] === "Y")
		{
			$pagerBaseLink = trim($arParams["PAGER_BASE_LINK"]);
			if ($pagerBaseLink === "")
			{
				if (
					$arResult["SECTION"]
					&& $arResult["SECTION"]["PATH"]
					&& $arResult["SECTION"]["PATH"][0]
					&& $arResult["SECTION"]["PATH"][0]["~SECTION_PAGE_URL"]
				)
				{
					$pagerBaseLink = $arResult["SECTION"]["PATH"][0]["~SECTION_PAGE_URL"];
				}
				elseif (
				$arItem["~LIST_PAGE_URL"]
				)
				{
					$pagerBaseLink = $arItem["~LIST_PAGE_URL"];
				}
			}

			if ($pagerParameters && isset($pagerParameters["BASE_LINK"]))
			{
				$pagerBaseLink = $pagerParameters["BASE_LINK"];
				unset($pagerParameters["BASE_LINK"]);
			}

			$navComponentParameters["BASE_LINK"] = CHTTP::urlAddParams($pagerBaseLink, $pagerParameters, array("encode"=>true));
		}

		$this->arParams = &$arParams;
		$this->arResult = &$arResult;
		$arResult["NAV_STRING"] = $rsElement->GetPageNavStringEx(
			$navComponentObject,
			$arParams["PAGER_TITLE"],
			$arParams["PAGER_TEMPLATE"],
			$arParams["PAGER_SHOW_ALWAYS"],
			$this,
			$navComponentParameters
		);
		$arResult["NAV_CACHED_DATA"] = null;
		$arResult["NAV_RESULT"] = $rsElement;
		return array($arParams, $arResult);

	}

	function ResultOption($arResult, $arParams, $USER, $APPLICATION, $INTRANET_TOOLBAR)
	{
		if(isset($arResult["ID"]))
		{
			$arTitleOptions = null;
			if($USER->IsAuthorized())
			{
				if(
					$APPLICATION->GetShowIncludeAreas()
					|| (is_object($GLOBALS["INTRANET_TOOLBAR"]) && $arParams["INTRANET_TOOLBAR"]!=="N")
					|| $arParams["SET_TITLE"]
				)
				{
					if(CModule::IncludeModule("iblock"))
					{
						$arButtons = CIBlock::GetPanelButtons(
							$arResult["IBLOCK_ID"],
							$arResult['ID'],
							'',
							array("SECTION_BUTTONS"=>false)
						);

						if($APPLICATION->GetShowIncludeAreas())
							$this->AddIncludeAreaIcons(CIBlock::GetComponentMenu($APPLICATION->GetPublicShowMode(), $arButtons));

						if(
							is_array($arButtons["intranet"])
							&& is_object($INTRANET_TOOLBAR)
							&& $arParams["INTRANET_TOOLBAR"]!=="N"
						)
						{
							$APPLICATION->AddHeadScript('/bitrix/js/main/utils.js');
							foreach($arButtons["intranet"] as $arButton)
								$INTRANET_TOOLBAR->AddButton($arButton);
						}

						if($arParams["SET_TITLE"])
						{
							$arTitleOptions = array(
								'ADMIN_EDIT_LINK' => $arButtons["submenu"]["edit_iblock"]["ACTION"],
								'PUBLIC_EDIT_LINK' => "",
								'COMPONENT_NAME' => $this->GetName(),
							);
						}
					}
				}
			}


			$this->SetTemplateCachedData($arResult["NAV_CACHED_DATA"]);

			if($arParams["SET_TITLE"])
			{
				if ($arResult["IPROPERTY_VALUES"] && $arResult["IPROPERTY_VALUES"]["SECTION_PAGE_TITLE"] != "")
					$APPLICATION->SetTitle($arResult["IPROPERTY_VALUES"]["SECTION_PAGE_TITLE"], $arTitleOptions);
				elseif(isset($arResult["NAME"]))
					$APPLICATION->SetTitle($arResult["NAME"], $arTitleOptions);
			}

			if ($arResult["IPROPERTY_VALUES"])
			{
				if ($arParams["SET_BROWSER_TITLE"] === 'Y' && $arResult["IPROPERTY_VALUES"]["SECTION_META_TITLE"] != "")
					$APPLICATION->SetPageProperty("title", $arResult["IPROPERTY_VALUES"]["SECTION_META_TITLE"], $arTitleOptions);

				if ($arParams["SET_META_KEYWORDS"] === 'Y' && $arResult["IPROPERTY_VALUES"]["SECTION_META_KEYWORDS"] != "")
					$APPLICATION->SetPageProperty("keywords", $arResult["IPROPERTY_VALUES"]["SECTION_META_KEYWORDS"], $arTitleOptions);

				if ($arParams["SET_META_DESCRIPTION"] === 'Y' && $arResult["IPROPERTY_VALUES"]["SECTION_META_DESCRIPTION"] != "")
					$APPLICATION->SetPageProperty("description", $arResult["IPROPERTY_VALUES"]["SECTION_META_DESCRIPTION"], $arTitleOptions);
			}
			if($arParams["ADD_ELEMENT_CHAIN"] == 'Y' && ($arResult["NAME"]))
			{
				$APPLICATION->AddChainItem($arResult["NAME"]);
			}

			if ($arParams["SET_LAST_MODIFIED"] && $arResult["ITEMS_TIMESTAMP_X"])
			{
				Context::getCurrent()->getResponse()->setLastModified($arResult["ITEMS_TIMESTAMP_X"]);
			}

			return $arResult["ELEMENTS"];
		}

	}

	function SetNavChain($arResult, $APPLICATION)
	{
		$dbSection = CIBlockSection::GetNavChain($arResult['IBLOCK_ID'], $arResult['IBLOCK_SECTION_ID'], array('NAME', 'CODE'));
		while ($arItem = $dbSection->fetch())
		{
			$APPLICATION->AddChainItem($arItem['NAME'], $arResult['LIST_PAGE_URL'].$arItem['CODE'].'/');
		}
	}

	function SetNavChainItem ($arResult, $APPLICATION)
	{
		$APPLICATION->AddChainItem($arResult['NAME'], $_SERVER['REDIRECT_URL']);
	}

	function GetNavChainParams ($arParams, $arSort, $arSearchFilter)
	{
		$arrFilter = $GLOBALS[$arParams["FILTER_NAME"]];
		if (!$arrFilter)
		{
			$arrFilter = array();
		}
		if (!$arSearchFilter)
		{
			$arSearchFilter = array();
		}
		$this->FilterList($arParams, $arrFilter, $arSearchFilter);
		list($arParams, $arFilter) = $this->FilterList($arParams, $arrFilter, $arSearchFilter);
		$rsElement = CIBlockElement::GetList($arSort, $arFilter, false, false, array('IBLOCK_ID', 'IBLOCK_SECTION_ID', 'NAME', 'LIST_PAGE_URL'));
		$arSetingNavChain = array();
		if ($arElement = $rsElement->fetch())
		{
			$arSetingNavChain['LIST_PAGE_URL'] = $arElement['LIST_PAGE_URL'];
			$arSetingNavChain['NAME'] = $arElement['NAME'];
			$arSetingNavChain['IBLOCK_SECTION_ID'] = $arElement['IBLOCK_SECTION_ID'];
			$arSetingNavChain['IBLOCK_ID'] = $arElement['IBLOCK_ID'];
		}
		return $arSetingNavChain;
	}

	public function executeComponent()
	{
		global $DB;
		global $USER;
		global $APPLICATION;
		global $INTRANET_TOOLBAR;

		CModule::IncludeModule('iblock');
		CPageOption::SetOptionString("main", "nav_page_in_session", "N");

		$arParams = $this->arParams;
		$arSearchFilter = $GLOBALS[$arParams['FILTER_SEARCH']];
		list ($arParams, $arrFilter, $pagerParameters, $bUSER_HAVE_ACCESS) = $this->SetParams($arParams, $DB, $USER);
		$arSort = $this->SortList($arParams['SORT_BY1'], $arParams['SORT_BY2'], $arParams['SORT_ORDER1'], $arParams['SORT_ORDER2']);
		if($this->StartResultCache(false, array(($arParams["CACHE_GROUPS"]==="N"? false: $USER->GetGroups()), $bUSER_HAVE_ACCESS,
			$arSearchFilter, $pagerParameters)))
		{
			if ($arParams['IBLOCK_CODE'])
			{
				list($arParams, $arFilter) = $this->FilterList($arParams, $arrFilter, $arSearchFilter);
				$arSelect = $this->SelectList($arParams['FIELD_CODE'], $arParams['PROPERTY_CODE']);
				$arGroup = $this->GroupList();
				list($arParams, $arResult) = $this->ExecuteElements($arParams, $arSort, $arFilter,
					$arGroup, array(), $arSelect, $pagerParameters, $bUSER_HAVE_ACCESS);
				$this->arResult = $arResult;

				if(count($arResult['ID']) <= 0)
				{
					$this->AbortResultCache();
					\Bitrix\Iblock\Component\Tools::process404(
						trim($arParams["MESSAGE_404"]) ?: GetMessage("T_NEWS_DETAIL_NF")
						,true
						,$arParams["SET_STATUS_404"] === "Y"
						,$arParams["SHOW_404"] === "Y"
						,$arParams["FILE_404"]
					);
				}
				$this->SetResultCacheKeys(array(
					"ID",
					"IBLOCK_CODE",
					"IBLOCK_TYPE_ID",
					"LIST_PAGE_URL",
					"NAV_CACHED_DATA",
					"NAME",
					"SECTION",
					"ELEMENTS",
					"IPROPERTY_VALUES",
					"ITEMS_TIMESTAMP_X",
				));
				$this->IncludeComponentTemplate();
			}
			else
			{
				$this->AbortResultCache();
				\Bitrix\Iblock\Component\Tools::process404(
					trim($arParams["MESSAGE_404"]) ?: GetMessage("T_NEWS_NEWS_NA")
					,true
					,$arParams["SET_STATUS_404"] === "Y"
					,$arParams["SHOW_404"] === "Y"
					,$arParams["FILE_404"]
				);
			}
		}
		if($arParams['ADD_SECTIONS_CHAIN'] == 'Y')
		{
			if (!$arResult)
			{
				$arSetingNavChain = $this->GetNavChainParams($arParams, $arSort, $arSearchFilter);
				$this->SetNavChain($arSetingNavChain, $APPLICATION);
			}
			else
			{
				$this->SetNavChain($arResult, $APPLICATION);
			}

		}
		if($arParams['ADD_ELEMENT_CHAIN'] == 'Y' && (!$arResult))
		{
			if (!$arSetingNavChain)
			{
				$arSetingNavChain = $this->GetNavChainParams($arParams, $arSort, $arSearchFilter);
			}
			$this->SetNavChainItem($arSetingNavChain, $APPLICATION);
		}
		CIBlockElement::CounterInc($arResult['ID']);
		$arResult['ELEMENTS'] = $this->ResultOption($arResult, $arParams, $USER, $APPLICATION, $INTRANET_TOOLBAR);
	}
}