<?
$MESS['IBLOCK_PARAMS'] = 'Information block parameters';
$MESS['IBLOCK_ID'] = 'Choose information block';
$MESS["IBLOCK_CACHE_FILTER"] = "Cash with filter?";
$MESS["IBLOCK_FIELD"] = "Fields";
$MESS["IBLOCK_SECTION_ID"] = "ID section";
$MESS["IBLOCK_SECTION_CODE"] = "SECTION CODE";
$MESS['FILTER_NAME'] = "Filter search's key";
$MESS['BN_P_IBLOCK_TYPE'] = 'Information block type';
$MESS["T_IBLOCK_DESC_ASC"] = "Sort Asc";
$MESS["T_IBLOCK_DESC_FID"] = "ID";
$MESS["T_IBLOCK_DESC_FNAME"] = "Name";
$MESS["T_IBLOCK_DESC_FACT"] = "Active from";
$MESS["T_IBLOCK_DESC_FSORT"] = "Sort";
$MESS["T_IBLOCK_DESC_FTSAMP"] = "Date of last change";
$MESS["T_IBLOCK_DESC_IBORD1"] = "First sorting field";
$MESS["T_IBLOCK_DESC_IBBY1"] = "Direction for first sorting";
$MESS["T_IBLOCK_DESC_IBORD2"] = "Second sorting field";
$MESS["T_IBLOCK_DESC_IBBY2"] = "Direction for second sorting";

$MESS["T_IBLOCK_DESC_LIST_ID"] = "Information block code";
$MESS["T_IBLOCK_DESC_LIST_TYPE"] = "Type of information block (used for verification only)";
$MESS["T_IBLOCK_DESC_LIST_PAGE_URL"] = "List page URL (from information block settings by default)";
$MESS["CP_BND_DETAIL_URL"] = "Detail page URL (from information block settings by default)";
$MESS["T_IBLOCK_DESC_INCLUDE_IBLOCK_INTO_CHAIN"] = "Include information block into navigation chain";
$MESS["T_IBLOCK_PROPERTY"] = "Properties";
$MESS["T_IBLOCK_DESC_KEYWORDS"] = "Set page keywords from property";
$MESS["T_IBLOCK_DESC_DESCRIPTION"] = "Set page description from property";
$MESS["IBLOCK_FIELD"] = "Fields";
$MESS["T_IBLOCK_DESC_ACTIVE_DATE_FORMAT"] = "Date display format";
$MESS["T_IBLOCK_DESC_USE_PERMISSIONS"] = "Use additional access restriction";
$MESS["T_IBLOCK_DESC_GROUP_PERMISSIONS"] = "User groups allowed to view detailed description";
$MESS["T_IBLOCK_DESC_PAGER_PAGE"] = "Page";
$MESS["T_IBLOCK_DESC_ADD_SECTIONS_CHAIN"] = "Add Section name to breadcrumb navigation";
$MESS["T_IBLOCK_DESC_ADD_ELEMENT_CHAIN"] = "Add element name to breadcrumbs";
$MESS["T_IBLOCK_DESC_CHECK_DATES"] = "Show only currently active elements";
$MESS["CP_BND_ELEMENT_CODE"] = "News code";
$MESS["CP_BND_ELEMENT_ID"] = "News ID";
$MESS["CP_BND_BROWSER_TITLE"] = "Set browser window title from property value";
$MESS["CP_BND_CACHE_GROUPS"] = "Respect Access Permissions";
$MESS["CP_BND_SET_META_KEYWORDS"] = "Set page keywords";
$MESS["CP_BND_SET_META_DESCRIPTION"] = "Set page description";
$MESS["CP_BND_SET_BROWSER_TITLE"] = "Set browser window title";
$MESS["CP_BND_SET_CANONICAL_URL"] = "Set canonical URL";
$MESS["CP_BND_SET_LAST_MODIFIED"] = "Set page last modified date to response header";
?>