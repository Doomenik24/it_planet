<?php
//ресайзим изображение
if ($arResult['DETAIL_PICTURE']) {
    $arFileTmp = CFile::ResizeImageGet(
        $arResult["DETAIL_PICTURE"],
        array(
            "width" => 500,
            "height" => 300),
        BX_RESIZE_IMAGE_EXACT,
        false,
        array() //убираем черный фон у прозрачных изображений
    );
    $arResult['DETAIL_PICTURE']['SRC'] = $arFileTmp['src'];
}