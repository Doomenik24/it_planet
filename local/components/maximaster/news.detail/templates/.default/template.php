<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="state_news content">
	<div class="state_news_title">
		<h2><?=$arResult['NAME'];?></h2>
	</div>

	<div class="state_news_detail_picture">
		<img src="<?=$arResult['DETAIL_PICTURE']['SRC'];?>">
	</div>

	<div class="state_news_detail_text">
		<?=$arResult['DETAIL_TEXT'];?>
	</div>
</div>