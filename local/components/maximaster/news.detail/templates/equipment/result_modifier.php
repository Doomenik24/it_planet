<?php
// ресайзим детальное изображение
if ($arResult['DETAIL_PICTURE']) {
    $arFileTmp = CFile::ResizeImageGet(
        $arResult["DETAIL_PICTURE"],
        array(
            "width" => 500,
            "height" => 300),
        BX_RESIZE_IMAGE_EXACT,
        false,
        array() //убираем черный фон у прозрачных изображений
    );
    $arResult['DETAIL_PICTURE']['SRC'] = $arFileTmp['src'];
}
// получаем и ресайзим доп. изображения
if ($arResult['PROPERTIES']['IMAGES']) {
    foreach ($arResult['PROPERTIES']['IMAGES']['VALUE'] as $nKey => $nIdPicture) {
        $arFile = CFile::GetFileArray($nIdPicture);
        if ($arFile) {
            $arFileTmp = CFile::ResizeImageGet(
                $arFile,
                array(
                    "width" => 500,
                    "height" => 300),
                BX_RESIZE_IMAGE_EXACT,
                false,
                array() //убираем черный фон у прозрачных изображений
            );
            $arResult['PROPERTIES']['IMAGES']['VALUE'][$nKey] = $arFileTmp['src'];
        }
    }
}