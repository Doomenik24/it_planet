<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="equipment">
    <div class="equipment_name">
        <?= $arResult['NAME']; ?>
    </div>

    <? if ($arResult['DETAIL_PICTURE']) : ?>
        <div class="equipment_detail_picture">
            <img src="<?= $arResult['DETAIL_PICTURE']['SRC']; ?>">
        </div>
    <? endif; ?>

    <div class="equipment_detail_text">
        <?= $arResult['DETAIL_TEXT']; ?>
    </div>

    <? if ($arResult['PROPERTIES']['LINK_VIDEO']['~VALUE']) : ?>
        <p>
            Видео по ссылке:
        </p>
        <?= $arResult['PROPERTIES']['LINK_VIDEO']['~VALUE']; ?>
    <? endif ?>

    <? if ($arResult['PROPERTIES']['VIDEO']['VALUE']['path']) : ?>
        <p>
            Добавленное видео:
        </p>
        <video width="<?= $arResult['PROPERTIES']['VIDEO']['VALUE']['width']; ?>"
               height="<?= $arResult['PROPERTIES']['VIDEO']['VALUE']['height']; ?>"
               controls>
            <source src="<?= $arResult['PROPERTIES']['VIDEO']['VALUE']['path']; ?>" type="video/mp4">
        </video>
    <? endif ?>

    <? if ($arResult['PROPERTIES']['IMAGES']['VALUE']) : ?>
        <div class="slider">
            <? foreach ($arResult['PROPERTIES']['IMAGES']['VALUE'] as $nKey => $sPathImages): ?>
                <div class="slide">
                    <img src="<?= $sPathImages ?>">
                </div>
            <? endforeach ?>
        </div>
    <? endif ?>
</div>