<?php

class playgroundsMap extends CBitrixComponent
{
    public function executeComponent()
    {
        CModule::IncludeModule('iblock');
        $cacheTime = 3600;
        if ($this->startResultCache($cacheTime)) {
            $contentMap = self::getContentMap();
            if (!$contentMap) {
                $this->abortResultCache();
                return;
            }

            if ($_REQUEST['action'] == 'getContentMap') {
                echo json_encode($contentMap);
                die();
            }

            $this->arResult = $contentMap;
        }
        $this->IncludeComponentTemplate();
    }

    public static function getContentMap()
    {
        $arFilter = Array("IBLOCK_ID" => IBLOCK_ID_PLAYGROUNDS, "ACTIVE" => "Y");

        $arSelect = Array(
            'ID',
            'NAME',
            'PREVIEW_TEXT',
            'PROPERTY_TYPE',
            'PROPERTY_ADDRESS',
            'PROPERTY_WORK_TIME',
            'PROPERTY_EMAIL'
        );
        $res = \CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
        $reg = '/^PROPERTY_(.*)_VALUE$/';

        while ($arPoint = $res->Fetch()) {
            foreach ($arPoint as $sCodeProp => $sValueProp) {
                if ($sValueProp) {
                    preg_match_all($reg, $sCodeProp, $out, PREG_SET_ORDER);
                    if ($out[0]) {
                        $arResult[$arPoint['ID']][$out[0][1]] = $sValueProp;
                    }

                    if ($sCodeProp == 'NAME') {
                        $arResult[$arPoint['ID']]['NAME'] = $sValueProp;
                    }
                    if ($sCodeProp == 'PREVIEW_TEXT') {
                        $arResult[$arPoint['ID']]['PREVIEW_TEXT'] = $sValueProp;
                    }
                }
            }

        }
        return $arResult;
    }

}