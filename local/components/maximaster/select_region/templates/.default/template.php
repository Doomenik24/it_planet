<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<select class="regions">
    <option value="<?=$arResult['FIRST_REGION']['ID']?>"><?= $arResult['FIRST_REGION']['NAME'] ?></option>
    <? foreach ($arResult['LIST_REGIONS'] as $nIdRegion => $arItem): ?>
        <option value="<?=$nIdRegion;?>"><?= $arItem['NAME']; ?></option>
    <? endforeach ?>
</select>
