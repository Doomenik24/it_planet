<?php

use Bitrix\Main;
use Bitrix\Main\Localization\Loc;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

Loc::loadMessages(__FILE__);
CModule::IncludeModule('iblock');

class SelectRegion extends \CBitrixComponent
{
    /**
     * Функция получает названия типов инфоблоков
     * @return array
     * @throws Main\ArgumentException
     */
    public function getRegion()
    {
        $arListRegions = array();

        $res = \Bitrix\Iblock\TypeTable::getList([
            array('*', 'LANG_MESSAGE')
        ]);
        // TODO: сделать проверку регуляркой, а не вручную писать, какие ИБ исключить
        while ($arItems = $res->Fetch()) {
            if ($arItems['IBLOCK_TYPE_LANG_MESSAGE_LANGUAGE_ID'] == LANGUAGE_ID
                && $arItems['IBLOCK_TYPE_LANG_MESSAGE_IBLOCK_TYPE_ID'] != 'state_news'
                && $arItems['IBLOCK_TYPE_LANG_MESSAGE_IBLOCK_TYPE_ID'] != 'equipment'
                && $arItems['IBLOCK_TYPE_LANG_MESSAGE_IBLOCK_TYPE_ID'] != 'sporting_regulations'
                && $arItems['IBLOCK_TYPE_LANG_MESSAGE_IBLOCK_TYPE_ID'] != 'gto'
                && $arItems['IBLOCK_TYPE_LANG_MESSAGE_IBLOCK_TYPE_ID'] != 'type_test'
            ) {
                $arListRegions['LIST_REGIONS'][$arItems['ID']]['ID'] = $arItems['ID'];
                $arListRegions['LIST_REGIONS'][$arItems['ID']]['NAME'] = $arItems['IBLOCK_TYPE_LANG_MESSAGE_NAME'];
            }

        }
        $arListRegions['FIRST_REGION'] = array_shift($arListRegions['LIST_REGIONS']);

        return $arListRegions;
    }

    public function executeComponent()
    {
        $this->arResult = $this->getRegion();

        $this->includeComponentTemplate();
    }
}