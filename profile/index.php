<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php"); ?>
<div class="content">
    <?
    if ($USER->IsAuthorized()) {
        $APPLICATION->SetTitle('Профиль');
        $APPLICATION->IncludeComponent("bitrix:main.profile", "profile", array(
            "AJAX_MODE" => "N",
            "AJAX_OPTION_SHADOW" => "Y",
            "AJAX_OPTION_JUMP" => "N",
            "AJAX_OPTION_STYLE" => "Y",
            "AJAX_OPTION_HISTORY" => "N",
            "SET_TITLE" => "Y",
            "USER_PROPERTY" => array(
                0 => "",
            ),
            "SEND_INFO" => "N",
            "CHECK_RIGHTS" => "N",
            "USER_PROPERTY_NAME" => ""
        ),
            false
        );
    } else {
        $APPLICATION->SetTitle('Авторизация');
        echo '<p>Для входа в свой профиль, пожалуйста, авторизуйтесь.</p>';
        $APPLICATION->IncludeComponent(
            "bitrix:system.auth.form",
            "profile",
            Array(
                "COMPONENT_TEMPLATE" => "profile",
                "FORGOT_PASSWORD_URL" => "/forgot_password/",
                "PROFILE_URL" => "/profile/",
                "REGISTER_URL" => "/register/",
                "SHOW_ERRORS" => "Y"
            )
        );
    }

    ?>
</div>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>
