<?
$arUrlRewrite = array(
	array(
		"CONDITION" => "#^/news/all_state_news/(.*)/(.*)/#",
		"RULE" => "ELEMENT_CODE=\$1",
		"ID" => "bitrix:news",
		"PATH" => "/news/all_state_news/index.php",
	),
	array(
		"CONDITION" => "#^/equipment/(.*)/(.*)/#",
		"RULE" => "ELEMENT_CODE=\$1",
		"ID" => "bitrix:news",
		"PATH" => "/equipment/index.php",
	),
	array(
		"CONDITION" => "#^/news/all_state_news/#",
		"RULE" => "",
		"ID" => "bitrix:news",
		"PATH" => "/news/all_state_news/index.php",
	),
	array(
		"CONDITION" => "#^/events/(.*)/(.*)/#",
		"RULE" => "ELEMENT_CODE=\$1",
		"ID" => "bitrix:news",
		"PATH" => "/events/index.php",
	),
	array(
		"CONDITION" => "#^/news/(.*)/(.*)/#",
		"RULE" => "ELEMENT_CODE=\$1",
		"ID" => "bitrix:news",
		"PATH" => "/news/index.php",
	),
	array(
		"CONDITION" => "#^/want_to_train/#",
		"RULE" => "",
		"ID" => "bitrix:forum",
		"PATH" => "/want_to_train/index.php",
	),
	array(
		"CONDITION" => "#^/equipment/#",
		"RULE" => "",
		"ID" => "bitrix:news",
		"PATH" => "/equipment/index.php",
	),
	array(
		"CONDITION" => "#^/equipment/(.*)/(.*)/#",
		"RULE" => "ELEMENT_CODE=\$1",
		"ID" => "bitrix:news",
		"PATH" => "/equipment/index.php",
	),
	array(
		"CONDITION" => "#^/events/#",
		"RULE" => "",
		"ID" => "bitrix:news",
		"PATH" => "/events/index.php",
	),
	array(
		"CONDITION" => "#^/news/#",
		"RULE" => "",
		"ID" => "bitrix:news",
		"PATH" => "/news/index.php",
	),
	array(
		"CONDITION" => "#^/sporting_regulations/#",
		"RULE" => "",
		"ID" => "bitrix:news.list",
		"PATH" => "/sporting_regulations/index.php",
	),
	array(
		"CONDITION" => "#^/gto/type_test/(.*)/(.*)#",
		"RULE" => "ELEMENT_CODE=$1",
		"ID" => "bitrix:news.detail",
		"PATH" => "/gto/type_test/index.php",
	)

);

?>