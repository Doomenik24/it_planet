<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php"); ?>
<div class="content">
    <h1>Площадки и секции</h1>
    <p style="text-align: center">
        Площадки  - метки с бордовым цветом.
        <br>
        Секции - метки с голубым цветом.
    </p>
    <? $APPLICATION->IncludeComponent("maximaster:playgrounds.map", ".default", array()); ?>
</div>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>
